FROM tomcat

RUN apt-get update -y

RUN apt-get install tomcat9-admin -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN apt-get install nano -y

COPY build/libs/devops-0.0.1-SNAPSHOT.war	/usr/local/tomcat/webapps/devops-0.0.1-SNAPSHOT.war

EXPOSE 8080